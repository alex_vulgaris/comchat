
#pragma once

#include "stdafx.h"
#include "Resource.h"

#include <map>

typedef std::map<int, int> MapPorts;

#include <sstream>

typedef std::basic_ostringstream<TCHAR, std::char_traits<TCHAR>, std::allocator<TCHAR> > OutputStringStream;

#include "..\COMPort\COMPort.h"


#define COMM_IN_QUEUE_SIZE          (2048 * sizeof(TCHAR))
#define COMM_OUT_QUEUE_SIZE         (2048 * sizeof(TCHAR))

#define MSG_MAX_LENGTH              (2048)
#define MSG_MAX_SIZE                (MSG_MAX_LENGTH * sizeof(TCHAR))

#define EDIT_INPUT_LIMITTEXT        (MSG_MAX_LENGTH - 1)

#define INCOMING_MSG_BUFFER_LENGTH  (MSG_MAX_LENGTH + 1)
#define OUTGOING_MSG_BUFFER_LENGTH  (MSG_MAX_LENGTH)
#define INCOMING_MSG_BUFFER_SIZE    (INCOMING_MSG_BUFFER_LENGTH * sizeof(TCHAR))
#define OUTGOING_MSG_BUFFER_SIZE    (OUTGOING_MSG_BUFFER_LENGTH * sizeof(TCHAR))

#define COMCHAT_ENTER_BEGIN_CHAR    ((CHAR)13)
#define COMCHAT_ENTER_END_CHAR      ((CHAR)10)
#define COMCHAT_EXIT_CHAR           ((CHAR)27)


INT_PTR CALLBACK DlgMainProc(HWND, UINT, WPARAM, LPARAM);
BOOL DlgMain_OnInitDialog(HWND, HWND, LPARAM);
void DlgMain_OnCommand(HWND, int, HWND, UINT);

INT_PTR CALLBACK DlgAboutProc(HWND, UINT, WPARAM, LPARAM);
BOOL DlgAbout_OnInitDialog(HWND, HWND, LPARAM);
void DlgAbout_OnCommand(HWND, int, HWND, UINT);

INT_PTR CALLBACK DlgConnectPortProc(HWND, UINT, WPARAM, LPARAM);
BOOL DlgConnectPort_OnInitDialog(HWND, HWND, LPARAM);
void DlgConnectPort_OnCommand(HWND, int, HWND, UINT);

void InitInputEdit(void);
void InitOutputEdit(void);

LRESULT CALLBACK EditInputProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK GetMessageProc(int, WPARAM, LPARAM);

void COMChatConnect(void);
void COMChatReconnect(void);
void COMChatDisconnect(void);
void COMChatExit(void);
void EditOutputSelectAll(void);
void EditOutputClear(void);
void HelpAbout(void);

void ConnectCOMPort(int);
void DisconnectCOMPort(void);
void EnumCOMPorts(HWND);

void UserConnect(void);
void UserDisconnect(void);

void ReceiveMessage(COMPort *, DWORD, LPVOID);
void TransmitMessage(void);

void OutputEditSetTextColor(COLORREF);
void OutputEditScrollToEnd(void);
void OutputEditWrite(COLORREF, LPCTSTR, LPCTSTR);
void OutputEditRightButtonDown(MSG *);
