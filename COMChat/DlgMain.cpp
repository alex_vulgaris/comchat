
#include "stdafx.h"
#include "COMChat.h"


extern HINSTANCE g_hInst;
extern HACCEL    g_hAccelTable;
extern HICON     g_hIconBig;
extern HICON     g_hIconSmall;

HWND     g_hWndMain;
HWND     g_hWndEditInput;
HWND     g_hWndEditOutput;
WNDPROC  g_lpfnDefEditInputProc;
HHOOK    g_hHook;
COMPort  g_COMPort;
MapPorts g_MapPorts;



INT_PTR CALLBACK DlgMainProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        HANDLE_MSG(hWnd, WM_INITDIALOG, DlgMain_OnInitDialog);
        HANDLE_MSG(hWnd, WM_COMMAND, DlgMain_OnCommand);
    }
    return (INT_PTR)(FALSE);
}


BOOL DlgMain_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam)
{
    g_hWndMain = hWnd;

    SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)(g_hIconBig));
    SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)(g_hIconSmall));

    InitInputEdit();
    InitOutputEdit();

    COMChatConnect();

    return TRUE;
}


void DlgMain_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT uCodeNotify)
{
    switch(id)
    {
    case IDC_ENTER:
        TransmitMessage();
        break;

    case ID_COMCHAT_CONNECT:
        COMChatConnect();
        break;

    case ID_COMCHAT_RECONNECT:
        COMChatReconnect();
        break;

    case ID_COMCHAT_DISCONNECT:
        COMChatDisconnect();
        break;

    case ID_COMCHAT_EXIT:
    case IDCANCEL:
        COMChatExit();
        break;

    case ID_EDIT_OUTPUT_SELECT_ALL:
        EditOutputSelectAll();
        break;

    case ID_EDIT_OUTPUT_CLEAR:
        EditOutputClear();
        break;

    case ID_HELP_ABOUT:
        HelpAbout();
        break;
    }
}



void InitInputEdit(void)
{
    g_hWndEditInput = GetDlgItem(g_hWndMain, IDC_EDIT_INPUT);

    SendMessage(g_hWndEditInput,
        EM_SETLIMITTEXT, EDIT_INPUT_LIMITTEXT, 0);

    g_lpfnDefEditInputProc = (WNDPROC)GetWindowLong(g_hWndEditInput, GWLP_WNDPROC);

    SetWindowLong(g_hWndEditInput, GWLP_WNDPROC, (LONG)(EditInputProc));

    g_hHook = SetWindowsHookEx(
        WH_GETMESSAGE,
        GetMessageProc,
        NULL,
        GetCurrentThreadId());
}


void InitOutputEdit(void)
{
    g_hWndEditOutput = GetDlgItem(g_hWndMain, IDC_EDIT_OUTPUT);

    SendMessage(g_hWndEditOutput,
        EM_SETBKGNDCOLOR, 0, (LPARAM)(RGB(200, 255, 200)));

    CHARFORMAT cf = { 0 };
    cf.cbSize    = sizeof(cf);
    cf.dwMask    = CFM_BOLD | CFM_SIZE | CFM_FACE;
    cf.dwEffects = CFE_BOLD;
    cf.yHeight   = 220;
    _tcscpy_s(cf.szFaceName, TEXT("Times New Roman Cyr"));

    SendMessage(g_hWndEditOutput,
        EM_SETCHARFORMAT, SCF_ALL, (LPARAM)(&cf));
}



LRESULT CALLBACK EditInputProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if(uMsg == WM_COMMAND)
    {
        switch(LOWORD(wParam))
        {
        case ID_EDIT_INPUT_SELECT_ALL:
            SendMessage(g_hWndEditInput, EM_SETSEL, 0, (LPARAM)(-1));
            return 0;

        case IDC_ENTER:
            SendMessage(g_hWndMain, WM_COMMAND, IDC_ENTER, 0);
            return 0;
        }
    }
    return CallWindowProc(g_lpfnDefEditInputProc, hWnd, uMsg, wParam, lParam);
}


LRESULT CALLBACK GetMessageProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    MSG *lpMsg = (MSG *)(lParam);
    if(lpMsg->hwnd == g_hWndEditInput)
    {
        if( lpMsg->message >= WM_KEYFIRST &&
            lpMsg->message <= WM_KEYLAST &&
            TranslateAccelerator(g_hWndEditInput, g_hAccelTable, lpMsg) )
        {
            lpMsg->message = WM_NULL;
        }
    }
    else if(lpMsg->hwnd == g_hWndEditOutput)
    {
        if(lpMsg->message == WM_RBUTTONDOWN)
        {
            OutputEditRightButtonDown(lpMsg);
        }
    }
    return CallNextHookEx(g_hHook, nCode, wParam, lParam);
}



void COMChatConnect(void)
{
    int nPort = (int)DialogBox(g_hInst,
        MAKEINTRESOURCE(IDD_CONNECT_PORT),
        g_hWndMain, DlgConnectPortProc);

    if(nPort != -1)
    {
        DisconnectCOMPort();
        ConnectCOMPort(nPort);
    }
}


void COMChatReconnect(void)
{
    int nPort = g_COMPort.GetPort();

    if(nPort != -1)
    {
        DisconnectCOMPort();
        ConnectCOMPort(nPort);
    }
    else
    {
        COMChatConnect();
    }
}


void COMChatDisconnect(void)
{
    DisconnectCOMPort();
}


void COMChatExit(void)
{
    DisconnectCOMPort();

    UnhookWindowsHookEx(g_hHook);
    SetWindowLong(g_hWndEditInput, GWLP_WNDPROC, (LONG)(g_lpfnDefEditInputProc));
    EndDialog(g_hWndMain, 0);
}


void EditOutputSelectAll(void)
{
    SendMessage(g_hWndEditOutput, EM_SETSEL, 0, (LPARAM)(-1));
    SetFocus(g_hWndEditOutput);
}


void EditOutputClear(void)
{
    SetWindowText(g_hWndEditOutput, NULL);
}


void HelpAbout(void)
{
    DialogBox(g_hInst,
        MAKEINTRESOURCE(IDD_ABOUT),
        g_hWndMain, DlgAboutProc);
}



void ConnectCOMPort(int nPort)
{
    TCHAR szText[256];

    if(g_COMPort.Open(nPort, COMM_IN_QUEUE_SIZE, COMM_OUT_QUEUE_SIZE))
    {
        LoadString(g_hInst, IDS_OPEN_PORT_MSG,
            szText, (sizeof(szText) / sizeof(TCHAR)));
        OutputEditWrite(
            RGB(0, 128, 0),
            NULL,
            szText);

        g_COMPort.SetReceiveDataCallback(ReceiveMessage);

        UserConnect();
    }
    else
    {
        TCHAR szCaption[100];
        LoadString(g_hInst, IDS_ERR_FAILED_TO_CONNECT,
            szText, (sizeof(szText) / sizeof(TCHAR)));
        LoadString(g_hInst, IDS_MSG_BOX_CAPTION,
            szCaption, (sizeof(szCaption) / sizeof(TCHAR)));

        MessageBox(NULL,
            szText, szCaption,
            MB_ICONINFORMATION);
    }
}


void DisconnectCOMPort(void)
{
    if(g_COMPort.IsOpen())
    {
        g_COMPort.SetReceiveDataCallback(NULL);

        UserDisconnect();

        g_COMPort.Close();

        TCHAR szText[256];
        LoadString(g_hInst, IDS_CLOSE_PORT_MSG,
            szText, (sizeof(szText) / sizeof(TCHAR)));
        OutputEditWrite(
            RGB(128, 128, 128),
            NULL,
            szText);
    }
}



void UserConnect(void)
{
    CHAR chChar = COMCHAT_ENTER_BEGIN_CHAR;
    g_COMPort.Write(&chChar, 1, 30);
}


void UserDisconnect(void)
{
    CHAR chChar = COMCHAT_EXIT_CHAR;
    g_COMPort.Write(&chChar, 1, 30);
}



void ReceiveMessage(COMPort *, DWORD cbBytesCount, LPVOID)
{
    if( !cbBytesCount )
    {
        return;
    }

    if(cbBytesCount > MSG_MAX_SIZE)
    {
        g_COMPort.ClearInQueue();
        return;
    }

    if(cbBytesCount == 1)
    {
        CHAR chChar = 0;
        if( !g_COMPort.Read(&chChar, 1, 30) )
        {
            return;
        }

        TCHAR szText[256];

        switch(chChar)
        {
        case COMCHAT_ENTER_BEGIN_CHAR:
            chChar = COMCHAT_ENTER_END_CHAR;
            g_COMPort.Write(&chChar, 1, 30);

        case COMCHAT_ENTER_END_CHAR:
            LoadString(g_hInst, IDS_ENTER_INTERLOCUTOR,
                szText, (sizeof(szText) / sizeof(TCHAR)));
            OutputEditWrite(
                RGB(0, 128, 128),
                NULL,
                szText);
            break;

        case COMCHAT_EXIT_CHAR:
            LoadString(g_hInst, IDS_EXIT_INTERLOCUTOR,
                szText, (sizeof(szText) / sizeof(TCHAR)));
            OutputEditWrite(
                RGB(128, 0, 128),
                NULL,
                szText);
            break;

        default:
            break;
        }

        return;
    }

    TCHAR szMsgText[INCOMING_MSG_BUFFER_LENGTH] = { 0 };

    if(g_COMPort.Read(szMsgText, cbBytesCount, 30))
    {
        TCHAR szNameInterlocutor[100];
        LoadString(g_hInst, IDS_USER_NAME_INTERLOCUTOR,
            szNameInterlocutor, (sizeof(szNameInterlocutor) / sizeof(TCHAR)));
        OutputEditWrite(
            RGB(128, 0, 0),
            szNameInterlocutor,
            szMsgText);
    }
}


void TransmitMessage(void)
{
    TCHAR szMsgText[OUTGOING_MSG_BUFFER_LENGTH] = { 0 };

    if( !GetWindowText(g_hWndEditInput, szMsgText, MSG_MAX_LENGTH) )
    {
        return;
    }

    if(g_COMPort.Write(szMsgText, ((_tcslen(szMsgText) + 1) * sizeof(TCHAR)), 30))
    {
        TCHAR szNameYou[100];
        LoadString(g_hInst, IDS_USER_NAME_YOU,
            szNameYou, (sizeof(szNameYou) / sizeof(TCHAR)));
        OutputEditWrite(
            RGB(0, 0, 128),
            szNameYou,
            szMsgText);

        SetWindowText(g_hWndEditInput, NULL);
    }
}



void OutputEditSetTextColor(COLORREF crColor)
{
    CHARFORMAT cf = { 0 };
    cf.cbSize      = sizeof(cf);
    cf.dwMask      = CFM_COLOR;
    cf.crTextColor = crColor;

    SendMessage(g_hWndEditOutput,
        EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)(&cf));
}


void OutputEditScrollToEnd(void)
{
    LONG_PTR nLineCount = SendMessage(g_hWndEditOutput,
        EM_GETLINECOUNT, 0, 0);

    SendMessage(g_hWndEditOutput,
        EM_LINESCROLL, 0, nLineCount);

    SendMessage(g_hWndEditOutput,
        EM_SCROLL, SB_LINEUP, 0);
}


void OutputEditWrite(COLORREF crColor, LPCTSTR lpUserName, LPCTSTR lpMsgText)
{
    SendMessage(g_hWndEditOutput,
        EM_SETSEL, (WPARAM)(-1), (LPARAM)(-1));

    OutputEditSetTextColor(crColor);

    SYSTEMTIME sysTime = { 0 };
    GetLocalTime(&sysTime);

    TCHAR szTime[32];
    wsprintf(szTime, TEXT("[%02hu:%02hu:%02hu] "),
        sysTime.wHour, sysTime.wMinute, sysTime.wSecond);

    OutputStringStream outStrStream;
    outStrStream << szTime;
    if(lpUserName)
    {
        outStrStream << TEXT("<") << lpUserName << TEXT("> ");
    }
    if(lpMsgText)
    {
        outStrStream << lpMsgText;
    }
    outStrStream << std::endl;

    SendMessage(g_hWndEditOutput,
        EM_REPLACESEL, FALSE, (LPARAM)(outStrStream.str().c_str()));

    OutputEditScrollToEnd();
}


void OutputEditRightButtonDown(MSG *lpMsg)
{
    TCHAR szSelectAll[100];
    TCHAR szClear[100];
    LoadString(g_hInst, IDS_EDIT_OUTPUT_SELECT_ALL,
        szSelectAll, (sizeof(szSelectAll) / sizeof(TCHAR)));
    LoadString(g_hInst, IDS_EDIT_OUTPUT_CLEAR,
        szClear, (sizeof(szClear) / sizeof(TCHAR)));

    HMENU hTrackMenu = CreatePopupMenu();

    AppendMenu(hTrackMenu, MF_STRING, ID_EDIT_OUTPUT_SELECT_ALL, szSelectAll);
    AppendMenu(hTrackMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hTrackMenu, MF_STRING, ID_EDIT_OUTPUT_CLEAR, szClear);

    TrackPopupMenu(hTrackMenu, 0,
        lpMsg->pt.x, lpMsg->pt.y,
        0, g_hWndMain, NULL);
}
