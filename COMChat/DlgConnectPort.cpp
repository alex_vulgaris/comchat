
#include "stdafx.h"
#include "COMChat.h"


extern HINSTANCE g_hInst;
extern HICON     g_hIconBig;
extern HICON     g_hIconSmall;
extern COMPort   g_COMPort;
extern MapPorts  g_MapPorts;

HWND g_hWndComboEnumPort;



INT_PTR CALLBACK DlgConnectPortProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        HANDLE_MSG(hWnd, WM_INITDIALOG, DlgConnectPort_OnInitDialog);
        HANDLE_MSG(hWnd, WM_COMMAND, DlgConnectPort_OnCommand);
    }
    return (INT_PTR)(FALSE);
}


BOOL DlgConnectPort_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam)
{
    SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)(g_hIconBig));
    SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)(g_hIconSmall));

    g_hWndComboEnumPort = GetDlgItem(hWnd, IDC_COMBO_ENUM_PORT);

    EnumCOMPorts(hWnd);

    return TRUE;
}


void DlgConnectPort_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT uCodeNotify)
{
    switch(id)
    {
    case IDOK:
        {
            int nIndex = (int)SendMessage(g_hWndComboEnumPort, CB_GETCURSEL, 0, 0);

            if(nIndex != -1)
            {
                EndDialog(hWnd, g_MapPorts[nIndex]);
            }
            else
            {
                TCHAR szText[256], szCaption[100];
                LoadString(g_hInst, IDS_ERR_PORT_IS_NOT_SELECTED,
                    szText, (sizeof(szText) / sizeof(TCHAR)));
                LoadString(g_hInst, IDS_MSG_BOX_CAPTION,
                    szCaption, (sizeof(szCaption) / sizeof(TCHAR)));

                MessageBox(NULL,
                    szText, szCaption,
                    MB_ICONINFORMATION);
            }
        }
        break;

    case IDCANCEL:
        EndDialog(hWnd, -1);
        break;
    }
}



void EnumCOMPorts(HWND hWnd)
{
    g_MapPorts.clear();

    DWORD dwGuidListSize = 0;
    SetupDiClassGuidsFromName(
        REGSTR_KEY_PORTS_CLASS,
        NULL,
        0,
        &dwGuidListSize);

    GUID *pGuidList = (GUID *)HeapAlloc(GetProcessHeap(),
        HEAP_ZERO_MEMORY, (sizeof(GUID) * dwGuidListSize));

    SetupDiClassGuidsFromName(
        REGSTR_KEY_PORTS_CLASS,
        pGuidList,
        dwGuidListSize,
        &dwGuidListSize);

    HDEVINFO hDevInfoSet = SetupDiGetClassDevs(
        pGuidList,
        NULL,
        NULL,
        DIGCF_PRESENT);

    HeapFree(GetProcessHeap(), 0, pGuidList);

    int nMemberIndex = 0;

    SP_DEVINFO_DATA devInfo = { 0 };
    devInfo.cbSize = sizeof(SP_DEVINFO_DATA);

    while(SetupDiEnumDeviceInfo(hDevInfoSet, nMemberIndex, &devInfo))
    {
        HKEY hDeviceKey = SetupDiOpenDevRegKey(
            hDevInfoSet,
            &devInfo,
            DICS_FLAG_GLOBAL,
            0,
            DIREG_DEV,
            KEY_QUERY_VALUE);

        if(hDeviceKey)
        {
            DWORD dwType = 0;
            DWORD dwDataSize = 0;

            TCHAR szPortName[256];
            dwDataSize = sizeof(szPortName);

            LSTATUS lStatus = RegQueryValueEx(
                hDeviceKey,
                REGSTR_VAL_PORTNAME,
                NULL,
                &dwType,
                (BYTE *)(szPortName),
                &dwDataSize);

            if( lStatus == ERROR_SUCCESS &&
                dwType == REG_SZ &&
                _tcslen(szPortName) > 3 &&
                !_tcsnicmp(szPortName, TEXT("COM"), 3) )
            {
                TCHAR szDescription[256];
                dwDataSize = sizeof(szDescription);

                SetupDiGetDeviceRegistryProperty(
                    hDevInfoSet,
                    &devInfo,
                    SPDRP_DEVICEDESC,
                    &dwType,
                    (BYTE *)(szDescription),
                    dwDataSize,
                    &dwDataSize);

                TCHAR szText[512];
                wsprintf(szText, TEXT("%s (%s)"), szPortName, szDescription);

                SendMessage(g_hWndComboEnumPort,
                    CB_ADDSTRING, 0, (LPARAM)(szText));

                int nCurNum = (int)SendMessage(g_hWndComboEnumPort,
                    CB_GETCOUNT, 0, 0) - 1;

                int nPort = _tstoi(szPortName + 3);
                g_MapPorts[nCurNum] = nPort;

                if(g_COMPort.GetPort() == nPort)
                {
                    SendMessage(g_hWndComboEnumPort,
                        CB_SETCURSEL, (WPARAM)(nCurNum), 0);
                }
            }

            RegCloseKey(hDeviceKey);
        }

        ++nMemberIndex;
    }

    SetupDiDestroyDeviceInfoList(hDevInfoSet);
}
