// COMChat.cpp: ���������� ����� ����� ��� ����������.
//

#include "stdafx.h"
#include "COMChat.h"


HINSTANCE g_hInst;
HACCEL    g_hAccelTable;
HICON     g_hIconBig;
HICON     g_hIconSmall;



int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE, LPTSTR, int)
{
    g_hInst = hInstance;

    HMODULE hModule = LoadLibrary(TEXT("riched20.dll"));
    g_hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDR_ACCELERATOR));
    g_hIconBig = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_BIG));
    g_hIconSmall = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_SMALL));

    DialogBox(hInstance,
        MAKEINTRESOURCE(IDD_MAIN),
        NULL, DlgMainProc);

    FreeLibrary(hModule);
    DestroyAcceleratorTable(g_hAccelTable);
    DestroyIcon(g_hIconBig);
    DestroyIcon(g_hIconSmall);

    return 0;
}
