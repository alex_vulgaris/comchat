//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by COMChat.rc
//
#define IDD_MAIN                        101
#define IDR_MENU                        102
#define IDR_ACCELERATOR                 103
#define IDD_ABOUT                       104
#define IDD_CONNECT_PORT                105
#define IDI_ICON_BIG                    106
#define IDI_ICON_SMALL                  107
#define IDS_ABOUT_DESCRIPTION           108
#define IDS_MSG_BOX_CAPTION             109
#define IDS_ERR_PORT_IS_NOT_SELECTED    110
#define IDS_ERR_FAILED_TO_CONNECT       111
#define IDS_OPEN_PORT_MSG               112
#define IDS_CLOSE_PORT_MSG              113
#define IDS_USER_NAME_INTERLOCUTOR      114
#define IDS_USER_NAME_YOU               115
#define IDS_ENTER_INTERLOCUTOR          116
#define IDS_EXIT_INTERLOCUTOR           117
#define IDS_EDIT_OUTPUT_SELECT_ALL      118
#define IDS_EDIT_OUTPUT_CLEAR           119
#define IDC_EDIT_INPUT                  1001
#define IDC_ENTER                       1002
#define IDC_EDIT_OUTPUT                 1003
#define IDC_EDIT_ABOUT                  1004
#define IDC_COMBO_ENUM_PORT             1005
#define ID_COMCHAT_CONNECT              40001
#define ID_COMCHAT_RECONNECT            40002
#define ID_COMCHAT_DISCONNECT           40003
#define ID_COMCHAT_EXIT                 40004
#define ID_EDIT_OUTPUT_SELECT_ALL       40005
#define ID_EDIT_OUTPUT_CLEAR            40006
#define ID_HELP_ABOUT                   40007
#define ID_EDIT_INPUT_SELECT_ALL        40008
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        120
#define _APS_NEXT_COMMAND_VALUE         40009
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
