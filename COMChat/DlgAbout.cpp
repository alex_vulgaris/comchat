
#include "stdafx.h"
#include "COMChat.h"


extern HINSTANCE g_hInst;
extern HICON     g_hIconBig;
extern HICON     g_hIconSmall;



INT_PTR CALLBACK DlgAboutProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        HANDLE_MSG(hWnd, WM_INITDIALOG, DlgAbout_OnInitDialog);
        HANDLE_MSG(hWnd, WM_COMMAND, DlgAbout_OnCommand);
    }
    return (INT_PTR)(FALSE);
}


BOOL DlgAbout_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam)
{
    SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)(g_hIconBig));
    SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)(g_hIconSmall));

    TCHAR szDescription[512];
    LoadString(g_hInst, IDS_ABOUT_DESCRIPTION,
        szDescription, (sizeof(szDescription) / sizeof(TCHAR)));

    SendDlgItemMessage(hWnd, IDC_EDIT_ABOUT,
        EM_REPLACESEL, FALSE, (LPARAM)(szDescription));

    return TRUE;
}


void DlgAbout_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT uCodeNotify)
{
    switch(id)
    {
    case IDOK:
    case IDCANCEL:
        EndDialog(hWnd, 0);
        break;
    }
}
