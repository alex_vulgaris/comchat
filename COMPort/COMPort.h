// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� COMPORT_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� COMPORT_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.

#pragma once

#ifdef COMPORT_EXPORTS
#define COMPORT_API __declspec(dllexport)
#else
#define COMPORT_API __declspec(dllimport)
#endif

#include "stdafx.h"


class COMPORT_API COMPort;

typedef void (*ReceiveDataCallback)(
    __in COMPort *lpCOMPort,
    __in DWORD cbBytesCount,
    __in LPVOID lpUserData);

typedef void (*TransmitDataCallback)(
    __in COMPort *lpCOMPort,
    __in LPVOID lpUserData);


class COMPORT_API COMPort
{

private:

    HANDLE m_hCOM;
    int    m_nPort;
    HANDLE m_hCommEvent;
    HANDLE m_hTerminateEvent;
    BOOL   m_bContinueListen;
    HANDLE m_hListenThread;

    ReceiveDataCallback m_lpfnRecvDataCall;
    LPVOID m_lpRecvUserData;
    TransmitDataCallback m_lpfnTranDataCall;
    LPVOID m_lpTranUserData;


public:

    COMPort(void);
    ~COMPort(void);


    BOOL Open(
        __in int nPort,
        __in DWORD cbInQueueSize,
        __in DWORD cbOutQueueSize);

    void Close(void);
    BOOL IsOpen(void) const;
    int GetPort(void) const;


    BOOL SetQueueSize(
        __in DWORD cbInQueueSize,
        __in DWORD cbOutQueueSize);

    BOOL SetState(
        __in DWORD dwBaudRate,
        __in BYTE cByteSize,
        __in BYTE cParity,
        __in BYTE cStopBits);

    BOOL SetStateDefault(void);

    BOOL SetTimeouts(
        __in DWORD dwReadIntervalTimeout,
        __in DWORD dwReadTotalTimeoutMultiplier,
        __in DWORD dwReadTotalTimeoutConstant,
        __in DWORD dwWriteTotalTimeoutMultiplier,
        __in DWORD dwWriteTotalTimeoutConstant);


    BOOL Read(
        __out_bcount(cbBufferSize) LPVOID lpBuffer,
        __in DWORD cbBufferSize,
        __in DWORD dwTimeout = INFINITE);

    BOOL Write(
        __in_bcount(cbBufferSize) LPCVOID lpBuffer,
        __in DWORD cbBufferSize,
        __in DWORD dwTimeout = INFINITE);

    BOOL TransmitChar(
        __in CHAR chChar);


    BOOL GetInQueueBytesCount(
        __out DWORD *lpBytesCount);

    BOOL GetOutQueueBytesCount(
        __out DWORD *lpBytesCount);


    BOOL CancelRead(void);
    BOOL CancelWrite(void);

    BOOL ClearInQueue(void);
    BOOL ClearOutQueue(void);


    void SetReceiveDataCallback(
        __in ReceiveDataCallback lpCallback,
        __in_opt LPVOID lpUserData = NULL);

    void SetTransmitDataCallback(
        __in TransmitDataCallback lpCallback,
        __in_opt LPVOID lpUserData = NULL);


private:

    BOOL OpenPort(
        __in int nPort);

    void ClosePort(void);

    BOOL InitPort(
        __in DWORD cbInQueueSize,
        __in DWORD cbOutQueueSize);

    void MakeStateDefault(
        __out DCB *lpDCB);


    BOOL CreateCommEvent(void);
    void CloseCommEvent(void);

    BOOL CreateTerminateEvent(void);
    void SetTerminateEvent(void);
    void CloseTerminateEvent(void);

    BOOL InitSync(void);
    void DeleteSync(void);

    BOOL BeginListenThread(void);
    void EndListenThread(void);


    static DWORD WINAPI ListenThread(
        __in LPVOID lpCOMPort);

    void ListenThread(void);

    void InvokeEventCallback(
        __in DWORD dwEvent);


    BOOL GetQueueBytesCount(
        __out DWORD *lpBytesCount,
        __in BOOL bInQueue);


}; // end class COMPort
