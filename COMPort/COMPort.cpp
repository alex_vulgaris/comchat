// COMPort.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "stdafx.h"
#include "COMPort.h"



COMPort::COMPort(void)
    : m_hCOM(INVALID_HANDLE_VALUE), m_nPort(-1),
      m_hCommEvent(), m_hTerminateEvent(),
      m_bContinueListen(), m_hListenThread(),
      m_lpfnRecvDataCall(), m_lpRecvUserData(),
      m_lpfnTranDataCall(), m_lpTranUserData()
{
}


COMPort::~COMPort(void)
{
    Close();
}



BOOL COMPort::OpenPort(int nPort)
{
    TCHAR szPortName[32];
    wsprintf(szPortName, TEXT("\\\\.\\COM%d"), nPort);

    m_hCOM = CreateFile(
        szPortName,
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_OVERLAPPED,
        NULL);

    if(m_hCOM == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

    m_nPort = nPort;
    return TRUE;
}


void COMPort::ClosePort(void)
{
    CloseHandle(m_hCOM);
    m_hCOM = INVALID_HANDLE_VALUE;

    m_nPort = -1;
}


BOOL COMPort::InitPort(DWORD cbInQueueSize, DWORD cbOutQueueSize)
{
    if( !SetupComm(m_hCOM, cbInQueueSize, cbOutQueueSize) )
    {
        return FALSE;
    }

    DCB dcb = { 0 };
    MakeStateDefault(&dcb);

    if( !SetCommState(m_hCOM, &dcb) )
    {
        return FALSE;
    }

    COMMTIMEOUTS commTimeouts = { 0 };

    if( !SetCommTimeouts(m_hCOM, &commTimeouts) )
    {
        return FALSE;
    }

    if( !SetCommMask(m_hCOM, EV_RXCHAR | EV_TXEMPTY) )
    {
        return FALSE;
    }

    if( !PurgeComm(m_hCOM, PURGE_TXCLEAR | PURGE_RXCLEAR) )
    {
        return FALSE;
    }

    return TRUE;
}


void COMPort::MakeStateDefault(DCB *lpDCB)
{
    lpDCB->DCBlength         = sizeof(DCB);
    lpDCB->BaudRate          = CBR_1200;
    lpDCB->fBinary           = TRUE;
    lpDCB->fParity           = FALSE;
    lpDCB->fOutxCtsFlow      = FALSE;
    lpDCB->fOutxDsrFlow      = FALSE;
    lpDCB->fDtrControl       = DTR_CONTROL_ENABLE;
    lpDCB->fDsrSensitivity   = FALSE;
    lpDCB->fTXContinueOnXoff = FALSE;
    lpDCB->fOutX             = FALSE;
    lpDCB->fInX              = FALSE;
    lpDCB->fErrorChar        = FALSE;
    lpDCB->fNull             = FALSE;
    lpDCB->fRtsControl       = RTS_CONTROL_ENABLE;
    lpDCB->fAbortOnError     = FALSE;
    lpDCB->fDummy2           = 0;
    lpDCB->wReserved         = 0;
    lpDCB->XonLim            = 2048;
    lpDCB->XoffLim           = 512;
    lpDCB->ByteSize          = 8;
    lpDCB->Parity            = NOPARITY;
    lpDCB->StopBits          = ONESTOPBIT;
    lpDCB->XonChar           = 17;
    lpDCB->XoffChar          = 19;
    lpDCB->ErrorChar         = 0;
    lpDCB->EofChar           = 0;
    lpDCB->EvtChar           = 0;
    lpDCB->wReserved1        = 0;
}



BOOL COMPort::CreateCommEvent(void)
{
    m_hCommEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

    if( !m_hCommEvent )
    {
        return FALSE;
    }
    return TRUE;
}


void COMPort::CloseCommEvent(void)
{
    CloseHandle(m_hCommEvent);
    m_hCommEvent = NULL;
}



BOOL COMPort::CreateTerminateEvent(void)
{
    m_hTerminateEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    if( !m_hTerminateEvent )
    {
        return FALSE;
    }
    return TRUE;
}


void COMPort::SetTerminateEvent(void)
{
    SetEvent(m_hTerminateEvent);
}


void COMPort::CloseTerminateEvent(void)
{
    CloseHandle(m_hTerminateEvent);
    m_hTerminateEvent = NULL;
}



BOOL COMPort::InitSync(void)
{
    if( !CreateCommEvent() )
    {
        return FALSE;
    }
    if( !CreateTerminateEvent() )
    {
        CloseCommEvent();
        return FALSE;
    }
    return TRUE;
}


void COMPort::DeleteSync(void)
{
    CloseTerminateEvent();
    CloseCommEvent();
}



BOOL COMPort::BeginListenThread(void)
{
    m_bContinueListen = TRUE;
    m_hListenThread = CreateThread(NULL, 0, ListenThread, this, 0, NULL);

    if( !m_hListenThread )
    {
        m_bContinueListen = FALSE;
        return FALSE;
    }
    return TRUE;
}


void COMPort::EndListenThread(void)
{
    m_bContinueListen = FALSE;
    SetTerminateEvent();

    WaitForSingleObject(m_hListenThread, INFINITE);
    CloseHandle(m_hListenThread);
    m_hListenThread = NULL;
}



BOOL COMPort::Open(int nPort, DWORD cbInQueueSize, DWORD cbOutQueueSize)
{
    Close();

    if( !OpenPort(nPort) )
    {
        return FALSE;
    }

    if( !InitPort(cbInQueueSize, cbOutQueueSize) )
    {
        ClosePort();
        return FALSE;
    }

    if( !InitSync() )
    {
        ClosePort();
        return FALSE;
    }

    if( !BeginListenThread() )
    {
        DeleteSync();
        ClosePort();
        return FALSE;
    }

    return TRUE;
}


void COMPort::Close(void)
{
    if( !IsOpen() )
    {
        return;
    }

    EndListenThread();
    DeleteSync();
    ClosePort();
}


BOOL COMPort::IsOpen(void) const
{
    return (m_hListenThread ? TRUE : FALSE);
}


int COMPort::GetPort(void) const
{
    return m_nPort;
}



BOOL COMPort::SetQueueSize(DWORD cbInQueueSize, DWORD cbOutQueueSize)
{
    if(IsOpen())
    {
        return SetupComm(m_hCOM, cbInQueueSize, cbOutQueueSize);
    }
    return FALSE;
}


BOOL COMPort::SetState(DWORD dwBaudRate, BYTE cByteSize, BYTE cParity, BYTE cStopBits)
{
    if( !IsOpen() )
    {
        return FALSE;
    }

    DCB dcb = { 0 };
    MakeStateDefault(&dcb);

    dcb.BaudRate = dwBaudRate;
    dcb.fParity  = (cParity != NOPARITY) ? TRUE : FALSE;
    dcb.ByteSize = cByteSize;
    dcb.Parity   = cParity;
    dcb.StopBits = cStopBits;

    return SetCommState(m_hCOM, &dcb);
}


BOOL COMPort::SetStateDefault(void)
{
    if( !IsOpen() )
    {
        return FALSE;
    }

    DCB dcb = { 0 };
    MakeStateDefault(&dcb);

    return SetCommState(m_hCOM, &dcb);
}


BOOL COMPort::SetTimeouts(
    DWORD dwReadIntervalTimeout,
    DWORD dwReadTotalTimeoutMultiplier,
    DWORD dwReadTotalTimeoutConstant,
    DWORD dwWriteTotalTimeoutMultiplier,
    DWORD dwWriteTotalTimeoutConstant)
{
    if( !IsOpen() )
    {
        return FALSE;
    }

    COMMTIMEOUTS commTimeouts = { 0 };
    commTimeouts.ReadIntervalTimeout         = dwReadIntervalTimeout;
    commTimeouts.ReadTotalTimeoutMultiplier  = dwReadTotalTimeoutMultiplier;
    commTimeouts.ReadTotalTimeoutConstant    = dwReadTotalTimeoutConstant;
    commTimeouts.WriteTotalTimeoutMultiplier = dwWriteTotalTimeoutMultiplier;
    commTimeouts.WriteTotalTimeoutConstant   = dwWriteTotalTimeoutConstant;

    return SetCommTimeouts(m_hCOM, &commTimeouts);
}



DWORD WINAPI COMPort::ListenThread(LPVOID lpCOMPort)
{
    ((COMPort *)(lpCOMPort))->ListenThread();
    return 0;
}


void COMPort::ListenThread(void)
{
    while(m_bContinueListen)
    {
        OVERLAPPED overlapped = { 0 };
        overlapped.hEvent = m_hCommEvent;

        DWORD dwEventMask = 0;
        WaitCommEvent(m_hCOM, &dwEventMask, &overlapped);

        HANDLE ahEvents[] = { m_hCommEvent, m_hTerminateEvent };
        DWORD dwRetVal = WaitForMultipleObjects(
            (sizeof(ahEvents)/sizeof(*ahEvents)), ahEvents, FALSE, INFINITE);

        switch(dwRetVal)
        {
        case WAIT_OBJECT_0:
            {
                DWORD dwBytes = 0;
                if(GetOverlappedResult(m_hCOM, &overlapped, &dwBytes, FALSE))
                {
                    InvokeEventCallback(dwEventMask);
                }
            }
            break;

        default:
            PurgeComm(m_hCOM, PURGE_TXABORT | PURGE_RXABORT);
            break;
        }
    }
}


void COMPort::InvokeEventCallback(DWORD dwEvent)
{
    switch(dwEvent)
    {
    case EV_RXCHAR:
        if(m_lpfnRecvDataCall)
        {
            DWORD cbBytesCount = 0;
            if(GetQueueBytesCount(&cbBytesCount, TRUE))
            {
                m_lpfnRecvDataCall(this, cbBytesCount, m_lpRecvUserData);
            }
        }
        break;

    case EV_TXEMPTY:
        if(m_lpfnTranDataCall)
        {
            m_lpfnTranDataCall(this, m_lpTranUserData);
        }
        break;

    default:
        break;
    }
}



BOOL COMPort::Read(LPVOID lpBuffer, DWORD cbBufferSize, DWORD dwTimeout)
{
    if( !IsOpen() )
    {
        return FALSE;
    }

    OVERLAPPED overlapped = { 0 };
    if( !(overlapped.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL)) )
    {
        return FALSE;
    }

    BOOL bResult = FALSE;

    DWORD dwBytesRead = 0;
    if( ReadFile(m_hCOM, lpBuffer, cbBufferSize, &dwBytesRead, &overlapped) &&
        dwBytesRead == cbBufferSize )
    {
        bResult = TRUE;
    }
    else
    {
        switch(WaitForSingleObject(overlapped.hEvent, dwTimeout))
        {
        case WAIT_OBJECT_0:
            if( GetOverlappedResult(m_hCOM, &overlapped, &dwBytesRead, FALSE) &&
                dwBytesRead == cbBufferSize )
            {
                bResult = TRUE;
            }
            break;

        case WAIT_TIMEOUT:
            PurgeComm(m_hCOM, PURGE_RXABORT);
            break;

        default:
            break;
        }
    }

    CloseHandle(overlapped.hEvent);
    return bResult;
}


BOOL COMPort::Write(LPCVOID lpBuffer, DWORD cbBufferSize, DWORD dwTimeout)
{
    if( !IsOpen() )
    {
        return FALSE;
    }

    OVERLAPPED overlapped = { 0 };
    if( !(overlapped.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL)) )
    {
        return FALSE;
    }

    BOOL bResult = FALSE;

    DWORD dwBytesWritten = 0;
    if( WriteFile(m_hCOM, lpBuffer, cbBufferSize, &dwBytesWritten, &overlapped) &&
        dwBytesWritten == cbBufferSize )
    {
        bResult = TRUE;
    }
    else
    {
        switch(WaitForSingleObject(overlapped.hEvent, dwTimeout))
        {
        case WAIT_OBJECT_0:
            if( GetOverlappedResult(m_hCOM, &overlapped, &dwBytesWritten, FALSE) &&
                dwBytesWritten == cbBufferSize )
            {
                bResult = TRUE;
            }
            break;

        case WAIT_TIMEOUT:
            PurgeComm(m_hCOM, PURGE_TXABORT);
            break;

        default:
            break;
        }
    }

    CloseHandle(overlapped.hEvent);
    return bResult;
}


BOOL COMPort::TransmitChar(CHAR chChar)
{
    if(IsOpen())
    {
        return TransmitCommChar(m_hCOM, chChar);
    }
    return FALSE;
}



BOOL COMPort::GetQueueBytesCount(DWORD *lpBytesCount, BOOL bInQueue)
{
    *lpBytesCount = 0;

    COMSTAT comStat = { 0 };
    if(ClearCommError(m_hCOM, NULL, &comStat))
    {
        *lpBytesCount = bInQueue ? comStat.cbInQue : comStat.cbOutQue;
        return TRUE;
    }
    return FALSE;
}


BOOL COMPort::GetInQueueBytesCount(DWORD *lpBytesCount)
{
    if(IsOpen())
    {
        return GetQueueBytesCount(lpBytesCount, TRUE);
    }
    return FALSE;
}


BOOL COMPort::GetOutQueueBytesCount(DWORD *lpBytesCount)
{
    if(IsOpen())
    {
        return GetQueueBytesCount(lpBytesCount, FALSE);
    }
    return FALSE;
}



BOOL COMPort::CancelRead(void)
{
    if(IsOpen())
    {
        return PurgeComm(m_hCOM, PURGE_RXABORT);
    }
    return FALSE;
}


BOOL COMPort::CancelWrite(void)
{
    if(IsOpen())
    {
        return PurgeComm(m_hCOM, PURGE_TXABORT);
    }
    return FALSE;
}


BOOL COMPort::ClearInQueue(void)
{
    if(IsOpen())
    {
        return PurgeComm(m_hCOM, PURGE_RXCLEAR);
    }
    return FALSE;
}


BOOL COMPort::ClearOutQueue(void)
{
    if(IsOpen())
    {
        return PurgeComm(m_hCOM, PURGE_TXCLEAR);
    }
    return FALSE;
}



void COMPort::SetReceiveDataCallback(ReceiveDataCallback lpCallback, LPVOID lpUserData)
{
    m_lpfnRecvDataCall = lpCallback;
    m_lpRecvUserData = lpUserData;
}


void COMPort::SetTransmitDataCallback(TransmitDataCallback lpCallback, LPVOID lpUserData)
{
    m_lpfnTranDataCall = lpCallback;
    m_lpTranUserData = lpUserData;
}
